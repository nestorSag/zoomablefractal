package org.sagn901212;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;

public class ApfloatCalculations implements Runnable{

  public Apfloat leftmost, rightmost, highest, lowest, width, height, hsteps, vsteps, minusOne, zero, one, two_, four, z_x, z_y, z_x_2, z_y_2, i_, j_,re, im, new_z_x;
  public MandelbrotDrawing draw_;
  public BufferedImage canvas;
  public long two;
  public int black, white, maxiter;

  public ApfloatCalculations(Apfloat leftmost_, Apfloat rightmost_, Apfloat highest_, Apfloat lowest_,MandelbrotDrawing draw, int precision){

    leftmost = leftmost_;
    rightmost = rightmost_;
    highest = highest_;
    lowest = lowest_;
    canvas = draw.canvas;
    width = new Apfloat((long) canvas.getWidth(),precision);
    height = new Apfloat((long) canvas.getHeight(),precision);
    hsteps = rightmost.add(leftmost.negate()).divide(width);
    vsteps = highest.add(lowest.negate()).divide(height);
    minusOne = new Apfloat((long) -1,precision);
    one = new Apfloat((long) 1,precision);
    zero = new Apfloat((long) 0,precision);
    four = new Apfloat((long) 4,precision);
    two_ = new Apfloat(two,precision);
    black = Color.BLACK.getRGB();
    white = Color.WHITE.getRGB();
    draw_ = draw;
    two = (long) 2;
    z_x = zero;
    z_x_2 = zero;
    z_y = zero;
    z_y_2 = zero;
    i_ = minusOne;
    j_ = minusOne;
    re = leftmost;
    im = highest;
    maxiter = draw.maxiter;
    new_z_x = zero;
    System.out.println("Precision: "+precision);
  }


  public void run(){
        for(int i=0;i<canvas.getWidth();i++){
            System.out.println("i: "+i);
            i_ = i_.add(one);
            j_ = minusOne;
            re = leftmost.add(hsteps.multiply(i_));
            for(int j=0;j<canvas.getHeight();j++){
                j_ = j_.add(one);
                im = highest.add(vsteps.negate().multiply(j_));
                int iter=0;
                while(z_x_2.add(z_y_2).compareTo(four)<=0 && iter<=maxiter){
                    z_x_2 = z_x.multiply(z_x);
                    z_y_2 = z_y.multiply(z_y);
                    new_z_x = z_x_2.add(z_y_2.negate()).add(re);
                    z_y = z_x.multiply(z_y).multiply(two_).add(im);
                    z_x = new_z_x;
                    iter=iter+1;
                }
                int pixColor = ((double) iter/ maxiter >=0.99)? black :draw_.colorScales[(int) Math.floor(draw_.colSteps*Math.pow((double) iter/maxiter,0.5))];
                canvas.setRGB(i,j,(int) pixColor);
                z_x = zero;
                z_y = zero;
                z_x_2 = zero;
                z_y_2 = zero;
            }
            j_ = minusOne;
        }
        System.out.println("Done.");
        draw_.canvas = canvas;
        draw_.repaint();
    }
}