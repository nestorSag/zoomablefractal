package org.sagn901212;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import org.apfloat.Apfloat;
import org.apfloat.ApfloatMath;


public class MandelbrotDrawing extends JPanel implements MouseListener
{
    public BufferedImage canvas;
    public int colSteps;
    public int[] colorScales;
    public boolean clicked;
    public Point clickedCoordinates;
    public Apfloat leftmost_, rightmost_, highest_, lowest_, epsilon;
    public JFrame frame;
    public int height_;
    public int maxiter;
    public int precision;
    
    public MandelbrotDrawing(double leftmost, double rightmost,
     double highest, double lowest){
        double hwRatio = (rightmost-leftmost)/(highest-lowest);
        height_=500;
        maxiter=5000;
        precision = 100;
        epsilon = new Apfloat(1e-6,precision);
        colSteps = 10000;
        int width =(int) Math.floor(height_*hwRatio);
        frame = new JFrame("Mandelbrot Set");
        ColorMaps colMap = new ColorMaps(colSteps);
        colorScales = 
            colMap.initColorMap(new Color(255,255,255),new Color(0,0,50),
                new Color(0,0,100),new Color(0,0,150),new Color(0,0,200),
                new Color(0,0,255),new Color(0,0,0));
        clicked = false;
        clickedCoordinates = null;
        canvas = new BufferedImage(width,height_,BufferedImage.TYPE_INT_ARGB);
        leftmost_ = new Apfloat(leftmost,precision);
        rightmost_ = new Apfloat(rightmost,precision);
        highest_ = new Apfloat(highest,precision);
        lowest_ = new Apfloat(lowest,precision);
        drawFractal(leftmost_.doubleValue(),rightmost_.doubleValue(),
            highest_.doubleValue(),lowest_.doubleValue());
        repaint();
        super.addMouseListener(this);

    }

    public void reScale(Apfloat newLeftmost, Apfloat newRightmost,
     Apfloat newHighest, Apfloat newLowest){
        Apfloat scale = 
        newRightmost.add(newLeftmost.negate()).divide(newHighest.add(
            newLowest.negate())).multiply(new Apfloat( height_,precision));
        BufferedImage newCanvas = new BufferedImage(
            scale.intValue(),height_,BufferedImage.TYPE_INT_ARGB);
        canvas = newCanvas;
        if(newRightmost.add(newLeftmost.negate()).compareTo(epsilon)<=0 
            || newHighest.add(newLowest.negate()).compareTo(epsilon)<=0){
            this.maxiter=1000;
            System.out.println(maxiter);
            ApfloatCalculations apCalc = new ApfloatCalculations(
                newLeftmost,newRightmost,newHighest,newLowest,this,precision);

            apCalc.run();
            canvas = apCalc.draw_.canvas;
            repaint();
        }else{
            maxiter=5000;
            drawFractal(newLeftmost.doubleValue(),newRightmost.doubleValue(),
                newHighest.doubleValue(),newLowest.doubleValue());
        }
        Apfloat center_x = newRightmost.add(newLeftmost).divide(
            new Apfloat(2.0,precision));
        Apfloat center_y = newHighest.add(newLowest).divide(
            new Apfloat(2.0,precision));
        String title = "Center: "+String.valueOf(center_x.doubleValue())+", "+
        String.valueOf(center_y.doubleValue());
        frame.setTitle(title);
        frame.setSize(canvas.getWidth(),canvas.getHeight());
        clicked = false;
        clickedCoordinates = null;
        leftmost_ = newLeftmost;
        rightmost_ = newRightmost;
        highest_ = newHighest;
        lowest_ = newLowest;
    }

    public void drawFractal(double leftmost, double rightmost, 
        double highest, double lowest){
        int height =canvas.getHeight();
        int width =canvas.getWidth();
        double hsteps = (rightmost-leftmost)/width;
        double vsteps = (highest-lowest)/height;
        int black = Color.BLACK.getRGB(), white = Color.WHITE.getRGB();;
        for(int i=0;i<canvas.getWidth();i++){
            double re = leftmost+hsteps*i;
            for(int j=0;j<canvas.getHeight();j++){
                double im = highest-vsteps*j;
                int iter=0;
                double z_x = 0.0;
                double z_y = 0.0;
                while(z_x*z_x+z_y*z_y<=4 && iter<=maxiter){
                    double new_z_x = z_x*z_x-z_y*z_y + re;
                    z_y = 2*z_x*z_y+im;
                    z_x = new_z_x;
                    iter=iter+1;
                }
                int pixColor = ((double) iter/(double) maxiter >=0.99)? 
                black :colorScales[(int) Math.floor(colSteps*Math.pow(
                    (double) iter/maxiter,0.5))];
                canvas.setRGB(i,j,(int) pixColor);
            }
        }
        repaint();
    }

    public void paintComponent(Graphics g)  // draw graphics in the panel
    {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(canvas, null, null);
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {
    
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getButton()==1){
            clickedCoordinates = e.getPoint();
            clicked = true;
        }else{
            if(e.getButton()==3){
                Apfloat hextra = rightmost_.add(leftmost_.negate()).divide(
                    new Apfloat(2.0,precision));
                Apfloat vextra = highest_.add(lowest_.negate()).divide(
                    new Apfloat(2.0,precision));
                reScale(leftmost_.add(hextra.negate()),rightmost_.add(hextra),
                    highest_.add(vextra),lowest_.add(vextra.negate()));
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e){
        if(e.getButton()==1){
            clickedCoordinates = e.getPoint();
            clicked = true;
        }else{
             if(e.getButton()==3){
                Apfloat hextra = rightmost_.add(leftmost_.negate()).divide(
                    new Apfloat(2.0,precision));
                Apfloat vextra = highest_.add(lowest_.negate()).divide(
                    new Apfloat(2.0,precision));
                reScale(leftmost_.add(hextra.negate()),rightmost_.add(hextra),
                    highest_.add(vextra),lowest_.add(vextra.negate()));
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e){
        int yCoord = e.getY();
        int xCoord = e.getX();
        int height, width;
        double newPxLeftmost, newPxRightmost, newPxLowest, newPxHighest;
        Apfloat hsteps, vsteps, newLeftmost, newRightmost, newLowest, newHighest, hwRatio;
        if(clicked && clickedCoordinates.x!=xCoord && clickedCoordinates.y!=yCoord){
            height = canvas.getHeight();
            width = canvas.getWidth();
            newPxLeftmost = clickedCoordinates.x<=xCoord? clickedCoordinates.x : xCoord;
            newPxRightmost = clickedCoordinates.x>xCoord? clickedCoordinates.x : xCoord;
            newPxLowest = clickedCoordinates.y>yCoord? clickedCoordinates.y : yCoord;
            newPxHighest = clickedCoordinates.y<=yCoord? clickedCoordinates.y : yCoord;
            hsteps = rightmost_.add(leftmost_.negate()).divide(new Apfloat(width,precision));
            vsteps = highest_.add(lowest_.negate()).divide(new Apfloat(height,precision));

            newLeftmost = leftmost_.add(hsteps.multiply(new Apfloat(newPxLeftmost,precision)));
            newRightmost = leftmost_.add(hsteps.multiply(new Apfloat(newPxRightmost,precision)));
            newLowest = highest_.add(vsteps.negate().multiply(new Apfloat(newPxLowest,precision)));
            newHighest = highest_.add(vsteps.negate().multiply(new Apfloat(newPxHighest,precision)));
            reScale(newLeftmost, newRightmost, newHighest, newLowest);
        }else{
            if(clickedCoordinates.x==xCoord && clickedCoordinates.y==yCoord){
                height = canvas.getHeight();
                width = canvas.getWidth();
                hsteps = rightmost_.add(leftmost_.negate()).divide(new Apfloat(width,precision));
                vsteps = highest_.add(lowest_.negate()).divide(new Apfloat(height,precision));
                Apfloat x = leftmost_.add(hsteps.multiply(new Apfloat (xCoord,precision)));
                Apfloat y = highest_.add(vsteps.negate().multiply(new Apfloat(yCoord,precision)));
                Apfloat vspace = highest_.add(lowest_.negate()).divide(new Apfloat(8.0,precision));
                Apfloat hspace = rightmost_.add(leftmost_.negate()).divide(new Apfloat(8.0,precision));
                newLeftmost = x.add(hspace.negate());
                newRightmost = x.add(hspace);
                newLowest = y.add(vspace.negate());
                newHighest = y.add(vspace);
                
                reScale(newLeftmost, newRightmost, newHighest, newLowest);
            }
        }
    }

    public static void main(String[] args)
    {
        double leftmost, rightmost, highest, lowest;
        if(args.length == 4){
            leftmost = Double.valueOf(args[0]); 
            rightmost = Double.valueOf(args[1]); 
            highest = Double.valueOf(args[2]); 
            lowest = Double.valueOf(args[3]);
        }else{
            System.out.println("args length is not 4. Creating default view");
            leftmost = -2.0;
            rightmost = 1.0; 
            highest = 1.0; 
            lowest = -1.0;
        }
        MandelbrotDrawing draw = new MandelbrotDrawing(leftmost,rightmost,highest,lowest);
                                 
        draw.frame.add(draw);
        draw.frame.pack();
        draw.frame.setSize(draw.canvas.getWidth(),draw.canvas.getHeight());
        draw.frame.setResizable(false);
        draw.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        draw.frame.setVisible(true);
    }

}